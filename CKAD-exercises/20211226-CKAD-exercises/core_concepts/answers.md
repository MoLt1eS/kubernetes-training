
## Start time: 15:11

## Answers

1. kubectl create ns mynamespace
2. kubectl run nginx --image=nginx -n mynamespace
   1. kubectl get pods -n mynamespace
   2. kubectl delete pod nginx -n mynamespace
3. kubectl run nginx --image=nginx -n mynamespace --dry-run=client -o yaml > 3.nginx.yaml && kubectl apply -f 3.nginx.yaml
   1. kubectl get pods -n mynamespace
   2. kubectl delete pod nginx -n mynamespace
   3. kubectl delete ns mynamespace
4. kubectl run busybox --image=busybox --restart=Never -it --rm -- env
5. kubectl run busybox --image=busybox --restart=Never --dry-run=client -o yaml -- env > 5.busybox.env
   1. kubectl logs busybox
   2. kubectl delete -f 5.busybox.env
6. kubectl create ns myrq --dry-run=client -o yaml
7. kubectl create quota myrq --hard="cpu=1,ram=1G,pods=2" --dry-run=client -o yaml 
8. kubectl get pods --all-namespaces
9. kubectl run nginx --image=nginx --port=80
10. kubectl edit pod nginx
    1.  kubectl set image pod/nginx nginx=nginx:1.7.1
    2.  kubectl get pod nginx
11. kubectl describe pod nginx
    1.  kubectl run tempbusybox --image=busybox -it --rm --restart=Never -- wget -O- 10.42.0.132
    2.  kubectl run tempbusybox --image=busybox -it --rm --restart=Never -- sh
        1.  wget -O- 10.42.0.132
12. kubectl get pod nginx -o yaml
13. kubectl describe pod nginx
14. kubectl logs nginx
15. kubectl logs nginx -p
16. kubectl exec -it nginx -- sh
17. kubectl run busybox --image=busybox -it --restart=Never -- echo "hello world"
    1.  kubectl logs busybox
    2.  kubectl delete pod busybox
18. kubectl run busybox --image=busybox --rm -it --restart=Never -- echo "hello world"
19. kubectl run nginx --image=nginx --env="var1=val1"
    1.  kubectl describe nginx


## End time: 15:32


# Summary

When defining the ResourceQuota instead of using --hard flag I was using the --quota, this lead me to check the final answer

Got no issues on the other questions