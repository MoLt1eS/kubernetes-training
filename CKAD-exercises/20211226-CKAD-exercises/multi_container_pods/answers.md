## Start time: 15:45

## Answers

### Create a Pod with two containers, both with image busybox and command "echo hello; sleep 3600". Connect to the second container and run 'ls'

- kubectl run busybox --image=busybox --restart=Never --dry-run=client -o yaml -- /bin/sh "echo hello; sleep 3600" > 1.busybox.yaml
- edit 1.busybox.yaml, duplicate pod definition and give it the name busybox2
- kubectl apply -f 1.busybox.yaml
- kubectl exec busybox -c busybox2 -it -- ls
- kubectl delete -f 1.busybox.yaml # cleanup

### Create pod with nginx container exposed at port 80. Add a busybox init container which downloads a page using "wget -O /work-dir/index.html http://neverssl.com/online". Make a volume of type emptyDir and mount it in both containers. For the nginx container, mount it on "/usr/share/nginx/html" and for the initcontainer, mount it on "/work-dir". When done, get the IP of the created pod and create a busybox pod and run "wget -O- IP"

- kubectl run nginx --image=nginx --port=80 --dry-run=client -o yaml > 2.nginx.yaml
- kubectl describe pod nginx
- kubectl run busybox --image=busybox --rm -it --restart=Never -- sh
- wget -O- <IP>

## End time: 16:00

# Summary

Issues defining the argument to run the wget command on the initContainer

Got no issues on the other questions