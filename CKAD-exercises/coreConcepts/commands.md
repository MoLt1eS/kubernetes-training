
### Create a namespace called 'mynamespace' and a pod with image nginx called nginx on this namespace

```bash
kubectl create namespace mynamespace
kubectl create pod nginx --image=nginx --restart=Never -n mynamespace
```

### Create the pod that was just described using YAML

Check file: nginx-pod.yaml

### Create a busybox pod (using kubectl command) that runs the command "env". Run it and see the output

```bash
kubectl run busybox --image=busybox --restart=Never -- env 
kubectl logs busybox
```

### Create a busybox pod (using YAML) that runs the command "env". Run it and see the output

Check file: busybox-pod.yaml

### Get the YAML for a new namespace called 'myns' without creating it

```bash
kubectl create namespace myns -o yaml --dry-run=client
```

### Get the YAML for a new ResourceQuota called 'myrq' with hard limits of 1 CPU, 1G memory and 2 pods without creating it

```bash
kubectl create quota myrq --hard="cpu=1,memory=1G,pods=2" --dry-run=client -o yaml
```

### Get pods on all namespaces

```bash
kubectl get pods --all-namespaces
```

### Create a pod with image nginx called nginx and expose traffic on port 80

```bash
kubectl run nginx --image=nginx --port=80
# Solution from website
# kubectl run nginx --image=nginx --restart=Never --port=80
# I don't think this requires the restart=Never since nginx it self is a service
```

### Change pod's image to nginx:1.7.1. Observe that the container will be restarted as soon as the image gets pulled

```bash
kubectl edit pod nginx
kubectl describe pod nginx # Check description
kubectl get pod # It should have 1 restart
# Another solution
# kubectl set image pod/nginx nginx=nginx:1.7.1
```

### Get nginx pod's ip created in previous step, use a temp busybox image to wget its '/'

```bash
kubectl describe pod nginx
# Fech the IP field, on this example: 10.42.0.77
# Start busybox container...
kubectl run -it --restart=Never busybox --image=busybox -- sh
$ wget 10.42.0.77
# Output:
# Connecting to 10.42.0.77 (10.42.0.77:80)
# saving to 'index.html'
# 'index.html' saved
$ exit
# Better solution:
kubectl run busybox --image=busybox --rm -it --restart=Never -- wget 10.1.1.131:80
```

### Get pod's YAML

```bash
kubectl get pod nginx -o yaml
```

### Get information about the pod, including details about potential issues (e.g. pod hasn't started)

```bash
kubectl describe pod nginx
```

### Get pod logs

```bash
kubectl logs nginx
```

### If pod crashed and restarted, get logs about the previous instance

```bash
# I had no idea on how to solve this one, solution from git:
kubectl logs nginx -p
```


### Execute a simple shell on the nginx pod

```bash
kubectl exec -it nginx -- sh
```

### Create a busybox pod that echoes 'hello world' and then exits

```bash
kubectl run -it busybox --image=busybox --restart=Never -- echo "hello world"
```

### Do the same, but have the pod deleted automatically when it's completed

```bash
kubectl run -it --rm busybox --image=busybox --restart=Never -- echo "hello world"
```

### Create an nginx pod and set an env value as 'var1=val1'. Check the env value existence within the pod

```bash
kubectl run nginx --image=nginx --env var1=val1
kubectl describe pod nginx 
```
