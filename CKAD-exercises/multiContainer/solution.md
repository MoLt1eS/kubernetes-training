### Create a Pod with two containers, both with image busybox and command "echo hello; sleep 3600". Connect to the second container and run 'ls'

```bash
kubectl run busybox --image=busybox --restart=Never --dry-run=client -o yaml -- /bin/sh -c 'echo hello; sleep 3600' > multipod.yaml
# Add the ' - -c ' on the args on the file
# Copy the container block and rename it to busybox2
kubectl apply -f multipod.yaml
kubectl exec busybox -c busybox2 -it -- ls
# Cleanup
kubectl delete -f multipod.yaml
```

### Create pod with nginx container exposed at port 80. Add a busybox init container which downloads a page using "wget -O /work-dir/index.html http://neverssl.com/online". Make a volume of type emptyDir and mount it in both containers. For the nginx container, mount it on "/usr/share/nginx/html" and for the initcontainer, mount it on "/work-dir". When done, get the IP of the created pod and create a busybox pod and run "wget -O- IP"

```bash
# Start by generating the template:
kubectl run nginx --image=nginx --port=80 --dry-run=client -o yaml > busyboxnginx.yaml
```

```yaml
# Add the following block
volumes:
    - name: data
  initContainers:
  - args:
    - /bin/sh
    - -c
    - wget -O /work-dir/index.html http://neverssl.com/online
    image: busybox
    name: busybox
    volumeMounts:
      - name:  data
        mountPath: /work-dir
# <snip>
# Mounting the volume on the nginx containter
# <snip>
    volumeMounts:
      - name:  data
        mountPath: /usr/share/nginx/html
```

```bash
# Fetch pod IP
kubectl describe nginx
# I had issues executing the "one liner command"
# Initial command:
# kubectl run box-test --image=busybox --restart=Never -it --rm -- /bin/sh -c "wget -O- IP"
# Due to errors:
kubectl run busyb --image=busybox --rm -it --restart=Never -- /bin/sh 
$ wget -O- 10.42.0.123
$ exit
```