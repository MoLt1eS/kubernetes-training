# Summary

My goal is to learn as much as possible about kubernetes and track the progress on this project

## Learning progress

- [x] Read the Kubernetes book
- [ ] Complete the [Udemy course on CKAD certification](https://www.udemy.com/course/certified-kubernetes-application-developer) (IN-PROGRESS)
- [ ] Complete all tasks from the [Kubernetes official docs](https://kubernetes.io/docs/tasks/)
- [ ] Complete all CKAD exercises from [GitHub](https://github.com/dgkanatsios/CKAD-exercises)
- [ ] Finish my personal TODO List

## CKAD exercises

Using the exercises from: https://github.com/dgkanatsios/CKAD-exercises

## Progress

- [x] https://github.com/dgkanatsios/CKAD-exercises/blob/master/a.core_concepts.md
- [x] https://github.com/dgkanatsios/CKAD-exercises/blob/master/b.multi_container_pods.md
- [ ] https://github.com/dgkanatsios/CKAD-exercises/blob/master/c.pod_design.md
- [ ] https://github.com/dgkanatsios/CKAD-exercises/blob/master/d.configuration.md
- [ ] https://github.com/dgkanatsios/CKAD-exercises/blob/master/e.observability.md
- [ ] https://github.com/dgkanatsios/CKAD-exercises/blob/master/f.services.md
- [ ] https://github.com/dgkanatsios/CKAD-exercises/blob/master/g.state.md

## Learn

- [ ] Kustomize
- [ ] Helm chart


## TODO List

- [ ] AuthN using AzureAD
- [ ] AuthZ using AzureAD groups
- [ ] K8s custom resources and custom controllers
- [ ] External DNS with PiHole
- [ ] Audit K8s cluster API and logs
- [ ] Chaos Monkey
- [ ] CIS, aquasec.com, kube-bench
