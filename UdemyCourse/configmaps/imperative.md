
## Create ConfigMaps imperative way

```bash
# Example to create configmaps via kubectl
$ kubectl create configmap myconfigma --from-literal="MyKey=ThisValue" --from-literal="AnotherKey=AnotherValue" --dry-run=client -o yaml
```