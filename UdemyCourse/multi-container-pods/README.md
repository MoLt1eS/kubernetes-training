

## Multi Container patterns

### Sidecar

For example: Log agent to convert and send to the central log server

### Ambassador

Have a pod to decide the database connectivity depending on the environment

You can outsource the configuration to another container that will proxy the request to the right database

## Exercise solution

1. kubectl get podd
1. kubectl describe pod blue (navy and teal)
1. kubectl run yellow --image=lemon --dry-run=client -o yaml > yellow-pod.yaml
1. kubectl get pods,svc -n elastic-stack
1. kubectl describe pod app -n elastic-stack
1. kubectl exec -it app /bin/sh cat /log/app.log
  1. kubectl get logs app
1. kubectl edit pod app


