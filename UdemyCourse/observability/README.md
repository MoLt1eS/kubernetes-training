# Observability

## Pod Status

When a Pod is created his first stats is `Pending`, this is where the schedules tries to find a node to place the Pod, if it can't find a place for it, it will remain in a `Pending` state. When the Pod is stuck on the pending state run the command:

```bash
$ kubectl describe pod <pod-name>
```

Once the Pod is scheduled to a node it changes to the `ContainerCreating` status where the images required for the application are pulled and the container starts. Onde all container in a Pod starts it goes into a `running` state where it continues to be until its `Terminated`

A Pod can only be at one status at the time, for more aditional information there's Pod conditions.

## Pod conditions

Conditions complement Pod status

For example, the Pod can be on a Running status but it's not yet Ready to accept incomming connections.

This is where Readiness Probe comes into play

### Readiness Probes

It will execute some task while comparing the output of that task with some result that will represent that the application is ready

Examples:

1. Call and http endpoint - /api/ready and expecting 200 OK
1. Testing for a TCP connectiong on a given port, example: TCP on port 3306
1. Executing a command or a script until it returns an okay status

#### Pod definition example

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myPod
  labels:
    name: myPod
spec:
  containers:
  - name: pod
    image: nginx
    ports:
      - containerPort: 80
    readinessProbe:
      httpGet:
        path: /api/ready
        port: 80
```

Other examples can be:

```yaml
    # TCP connection
    readinessProbe:
      tcpSocket:
        port: 80

    # Exec command
    readinessProbe:
      exec:
        command:
          - cat
          - /app/is_ready
```

If your application takes about 10 seconds to warm up it's possible to add that delay to the probe using the `initialDelaySeconds`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myPod
  labels:
    name: myPod
spec:
  containers:
  - name: pod
    image: nginx
    ports:
      - containerPort: 80
    readinessProbe:
      httpGet:
        path: /api/ready
        port: 80
    # Adding a 10 second delay
    initialDelaySeconds: 10
    # Period to probe
    periodSeconds: 5
    # Increase de default probe tries (default: 3)
    failureThreshold: 8
```



## Liviness Probes

When the container is in the Running state your application might have issues but the Pod continues on the Running state.. Liviness probe helps to solve this issues by checking the container with a command and expecting a valid response, otherwise it will attempt to restart



```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myPod
  labels:
    name: myPod
spec:
  containers:
  - name: pod
    image: nginx
    ports:
      - containerPort: 80
    livenessProbe:
      httpGet:
        path: /api/ready
        port: 80
    # Adding a 10 second delay
    initialDelaySeconds: 10
    # Period to probe
    periodSeconds: 5
    # Increase de default probe tries (default: 3)
    failureThreshold: 8
```



## Container Logging

kubectl logs -f <pod_name>

If there's multiple containers in the pod:

kubectl logs -f <pod_name> -c <container_name>


## Monitor and Debug Applications


https://github.com/kubernetes-sigs/metrics-server


I preffer prometheus
