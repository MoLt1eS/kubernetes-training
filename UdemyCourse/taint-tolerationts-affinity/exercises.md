# Taint and tolerations

Node affinity is a property of Pods that attracts them to a set of nodes (either as a preference or a hard requirement). Taints are the opposite -- they allow a node to repel a set of pods.

Tolerations are applied to pods, and allow (but do not require) the pods to schedule onto nodes with matching taints.

Taints and tolerations work together to ensure that pods are not scheduled onto inappropriate nodes. One or more taints are applied to a node; this marks that the node should not accept any pods that do not tolerate the taints.

Source: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/

### How to tain a node

```bash
kubectl taint nodes <node_name> <key>=<value>:<taint-effect>
```

The `key`, `value` and `taint-effect` are used on the pod toleration configuration, example:

```bash
kubectl taint nodes node1 app=blue:NoSchedule
```

#### Pod

```yaml
apiVersion:
kind: Pod
metadata:
  name: myapp-pod
spec:
  tolerations:
- key: "app"
  operation: "Equal"
  value: "blue"
  effect: "NoSchedule"
  containers:
  - name: nginx-container
    image: nging

```

#### Taints and Tolerations exercise solution (12 Questions)


1. $ kubectl get nodes --no-headers | wc -l
2. $ kubectl describe nodes node01 | grep taint
3. $ kubectl taint nodes node02 spray=mortein:NoSchedule
4. $ kubectl run mosquito --image=nginx
5. $ kubectl get pods mosquito
   1. Pending state
6. $ kubectl describe pod mosquito
   1. Check Events
   2. Does not tolerate tains
7. kubectl run bee --image=nginx --dry-run=client -o yaml > bee.yaml
   1. Add the snipet of toleration
   2. toleration: - key: "spray" value: "mortein" operator: "Equal" effect: "NoSchedule"
8. << No question >>
9. kubectl describe node master | grep taint
10. kubectl taint nodes master <copy_last_output>-
11. kubectl get pod mosquito
12. master
    1.  because we removed the taint from the master node

# Node selectors


## Pod definition

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pid
spec:
  containers:
    name: data-processor
    image: data-processor
  nodeSelector:
    size: Large
```

# Node label

```bash
kubectl labels nodes <node-name> <label-key>:<label-value>
# Example for the previous pod definition
kubectl label nodes <node-name> size:Large
```

Node selector contains limitations, using complex requirements such as "place the pod on Large or Medium nodes" or "Not Small" nodes

This is where Node Affinity comes into place


# Node affinity

Primary propose is to ensure that pods are hosted on particular nodes

Using the previous example, now using node affinity:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pid
spec:
  containers:
    name: data-processor
    image: data-processor
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: size
            operator: In
            values:
            - Large
```

Let's say that the pod can be on Large or Medium nodes

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pid
spec:
  containers:
    name: data-processor
    image: data-processor
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: size
            operator: In
            values:
            - Large
            - Medium
```

Instead of limiting to Large and Medium, lets change it to not run on any Small node

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pid
spec:
  containers:
    name: data-processor
    image: data-processor
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: size
            operator: NotIn
            values:
            - Small
```

## Node Affinity types

- `requiredDuringSchedulingIgnoredDuringExecution`
- `preferredDuringSchedulingIgnoredDuringExecution`
  
- `requiredDuringSchedulingRequiredDuringExecution`

# References

- https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/
- https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes-using-node-affinity/
  
#### Node Affinity exercise solution (8 Questions)

1. kubectl get nodes node01 --show-labels
2. kubectl get nodes node01 --show-labels
3. kubectl label nodes node01 color:blue
4. kubectl create deployment blue --image=nginx --replicas=6 --dry-run=client -o yaml
5. kubectl get pods -o wide
6. edit deployment and add affinity
   1. kubectl get deployment blue
   2. kubectl edit deployment blue
   3. check the blue-deployment.yaml file
7. kubectl get pods -o wide
8. kubectl create deployment red --image=nginx --replicas=3 --dry-run=client -o yaml > red-deployment.yaml



## Taints & Tolerations vs Node Affinity


Pods: ["Green", "Blue", "Red", "Grey", "Grey"]
Nodes: ["NodeGreen", "NodeBlue", "NodeRed", "NodeOther", "NodeOther"]

### Using taints and tolerations

Taint: NodeBlue with Color=Blue:NoSchedule
Taint: NodeGreen with Color=Green:NoSchedule
Taint: NodeRed with Color=Red:NoSchedule

Set toleration on the pods for the respective colors

The issue is that this will not garantee that the pods are going to the right nodes

Taints and tolerations doesn't solve this issue

### Node Affinity

Using the same approach gives another problem, this will not garantee that the GreyPods will not use the ColoredNodes

### Taints & Tolerations vs Node Affinity

By using the combination of both Tains, Tolerations and Node affinity solves the issues of making sure only the colored pods goes to the colored nodes and the ones without color will not be deployed on those nodes

